package Biblio;

/**
 *
 * @author Alexis
 */
public class EltMM {
    
    private String titre;
    private int duree;
    private boolean presentEnRayon;
    private String commentaires;
    
    /**
     * Initialise un Element Multimedia
     * @param unTitre Le titre de l'element multimedia
     * @param uneDuree La durée de l'element multimedia
     */
    public EltMM(String unTitre, int uneDuree){
        this.titre = unTitre;
        this.duree = uneDuree;
        this.presentEnRayon = false;
        this.commentaires = "<pas de commentaires>";
    }
    
    /**
     * Donne le titre de l'element multimedia
     * @return titre de l'element.
     */
    public String donneTitre() {
        return this.titre;
    }
    
    /**
     * Donne la durée de l'element multimedia
     * @return durée de l'element multimedia
     */
    public int donneDuree() {
        return this.duree;
    }
    
    /**
     * Fixe l'indicateur pour indiquer si l'element multimedia
     * est dans la bibliotheque.
     * @param etat true si l'element multimedia est en rayon, false autrement.
     */
    public void changeEtatRayon(boolean etat) {
        this.presentEnRayon = etat;
    }
    
    /**
     * @return true si l'element multimedia est en rayon.
     */
    public boolean donneEtatRayon() {
        return this.presentEnRayon;
    }
    
    /**
     * Ajoute un commentaire a l'element multimedia
     * @param commentaires Les commentaires devant etre ajoutes.
     */
    public void ajouteCommentaires(String comment) {
        this.commentaires = comment;
    }
    
    /**
     * Donne les commentaires relatif a l'element multimedia
     * @return Les commentaires de ce DVD.
     */
    public String donneCommentaires() {
        return this.commentaires;
    }
    
    /**
     * Retourne une chaîne de caractères indiquant le type de l’objet
     * @return le type de l'objet sous chaine de caractère
     */
    public String donneType(){
        return "EltMM";
    }
    
    /**
     * Renvoie une description textuelle de l'objet
     * @return un chaine de caracteres decrivant l'objet
     */
    public String toString() {
        String s = "";
        s += "Titre : " + this.titre + "\n";
        s += "Duree : " + this.duree + "\n";
        s += "Etat : ";
        if (this.presentEnRayon) {
            s += "disponible\n";
        } else {
            s += "emprunte\n";
        }
        s += "Commentaires : " + this.commentaires + "\n";
        return s;
    }
}
