package Biblio;

import java.util.ArrayList;

/**
 * La classe BiblioMM fournit un moyen de stocker des objets
 * CD et DVD. Une liste de tous les CD et DVD peut etre affichee
 * en mode texte.
 * 
 */
public class BiblioMM
{
    private ArrayList<EltMM> eltmms;

    /**
     * Construit une bibliotheque vide.
     */
    public BiblioMM()
    {
        this.eltmms = new ArrayList<EltMM>();
    }
  
    /**
     * Ajoute un EltMM a la bibliotheque.
     * @param unelt l'élément ajouter.
     */
    public void ajouterElt(EltMM elt)
    {
        this.eltmms.add(elt);
    }

    /**
     * Affiche une liste de tous les CD et DVD actuellement dans
     * la bibliotheque.
     */
    public void affiche()
    {
        for(EltMM eltmm : this.eltmms) {
            System.out.println(eltmm);   
        }
    }
    
    /**
     * Renvoie tous les elements qui correspondent au titre
     * @param titre le titre référence
     * @return une liste des elements qui correspondent au titre
     */
    public ArrayList<EltMM> rechercherTitre(String titre){
        ArrayList<EltMM> eltTitre = new ArrayList<EltMM>();
        
        for (int i = 0; i < this.eltmms.size(); i++) {
            if(this.eltmms.get(i).donneTitre().equals(titre)){
                eltTitre.add(this.eltmms.get(i));
            }
        }
        return eltTitre;
    }
    
    /**
     * Permet d’emprunter un élément multimédia. L’emprunt ne pourra se faire
     * que si le résultat de la recherche est constitué d’1 seul élément et si
     * celui-ci est disponible.
     * @param titre de l'element à emprunter
     * @return true si l'emprunt est possible, false sinon
     */
    public boolean emprunterTitre(String titre){
        boolean emprunt = false;
        ArrayList<EltMM> listeTitre = rechercherTitre(titre);
        
        if(listeTitre.size() == 1 && listeTitre.get(0).donneEtatRayon()) {
            emprunt = true;
            listeTitre.get(0).changeEtatRayon(false);
        } 
        return emprunt;
    }
}
