package Biblio;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class TestMultimedia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Création initiale
        BiblioMM biblio = new BiblioMM();
        
        EltMM dvd = new DVD("Toast", "Moi", 120);
        EltMM cd = new CD("Toast song 2000", "Lui", 23, 60);
        
        biblio.ajouterElt(dvd);
        biblio.ajouterElt(cd);
        
        //Test de recherche de titre
        System.out.println(biblio.rechercherTitre("Toast song 2000"));
        
        //Test d'emprunts successifs
        System.out.println("\n");
        System.out.println(biblio.emprunterTitre("Toast song 2000")); //false
        System.out.println(biblio.emprunterTitre("Toast song 2000")); //false
        cd.changeEtatRayon(true);
        System.out.println(biblio.emprunterTitre("Toast song 2000")); //true
        System.out.println(biblio.emprunterTitre("Toast song 2000")); //false
        
        //Test donneType
        EltMM dvd2 = new DVD("Un truc cool", "Moi", 120);
        EltMM cd2 = new CD("Un truc cool", "Lui", 23, 60);
        biblio.ajouterElt(dvd2);
        biblio.ajouterElt(cd2);
        
        System.out.println(dvd2.donneType());
        System.out.println(cd2.donneType());
        
        ArrayList<EltMM> test = biblio.rechercherTitre("Un truc cool");
        
        for (int i = 0; i < test.size(); i++) {
            if(test.get(i).donneType().equals("DVD")){
                System.out.println(((DVD) test.get(i)).donneRealisateur());
            }
            if(test.get(i).donneType().equals("CD")){
                System.out.println(((CD) test.get(i)).donneArtiste());
            }
        }    
    }
}
