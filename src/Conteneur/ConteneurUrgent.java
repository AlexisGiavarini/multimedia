package Conteneur;

/**
 *
 * @author Alexis
 */

/**
 * 
 * Représente un conteneur aérien pouvant contenir des colis
 */
public class ConteneurUrgent extends Conteneur{
    private int poidsMax;
    
    /**
     * Créé un Conteneur urgent (par voie aérienne)
     * @param distance distance à parcourir
     * @param volumeMax Volume maximal des colis
     * @param poidsMax Poids maximal des colis
     */
    public ConteneurUrgent(int distance, int volumeMax, int poidsMax){
        super(distance, volumeMax);
        this.poidsMax = poidsMax;
    }
    
    /**
     * Verifie que le colis respecte les contraintes de volume et de poids
     * @param c le colis à tester
     * @return true si le colis peut être ajouté dans le conteneur
     */
    public boolean conditionChargementUrgent(Colis c){
        if(super.conditionChargement(c)){
            if(super.donnePoids() + c.donnePoids() <= this.poidsMax){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public String toString(){
        return "\nPoids maximal : "+this.poidsMax+"\n"+super.toString();
    }
}
