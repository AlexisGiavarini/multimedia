package Conteneur;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * 
 * Représente un conteneur pouvant contenir des colis
 */
public class Conteneur {
    private int distance;
    private int volumeMax;
    private int volumeCourant;
    private int poids;
    private ArrayList<Colis> listeColis;
    
    /**
     * Crée un conteneur
     * @param distance distance à parcourir
     * @param volumeMax volume maximal des colis
     */
    public Conteneur(int distance, int volumeMax){
        this.distance = distance;
        this.volumeMax = volumeMax;
        this.volumeCourant = 0;
        this.poids = 0;
        this.listeColis = new ArrayList<Colis>();
    }
    
    /**
     * Permet d'ajouter un colis dans le conteneur
     * @param c le colis à ajouter
     * @return true si le colis a pu être ajouté
     */
    public boolean ajout(Colis c){      
        if(this.conditionChargement(c)){
            this.listeColis.add(c);
            this.poids += c.donnePoids();
            this.volumeCourant += c.donneVolume();
            return true;
        }else{
            return false;
        }       
    }
    
    /**
     * Calcul du cout de transport du conteneur : distance * poids
     * @return le cout du transport
     */
    public int cout(){
        return this.distance * this.poids;
    }
    
    /**
     * Verifie que le colis respecte les contraintes du conteneur
     * @param c le colis à tester
     * @return true si le colis peut etre ajouté dans le conteneur
     */
    public boolean conditionChargement(Colis c){
        if(this.volumeCourant + c.donneVolume() <= this.volumeMax){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Retourne le volume courant occupé
     * @return le volume courant
     */
    public int donneVolume(){
        return this.volumeCourant;
    }
    
    /**
     * Retourne la distance à parcourir
     * @return la distance
     */
    public int donneDistance(){
        return this.distance;
    }
    
    /**
     * Retourne le poids courant du conteneur
     * @return le poids courant
     */
    public int donnePoids(){
        return this.poids;
    }
    
    public String toString(){
        return "Distance à parcourir : "+this.distance
                +"\nVolume maximal : "+this.volumeMax
                +"\nVolume courant : "+this.volumeCourant
                +"\nPoids courant : "+this.poids
                +"\nListe colis : "+this.listeColis.toString();
    }
}
