package Conteneur;

/**
 *
 * @author Alexis
 */
public class TestConteneur {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Création initiale Conteneur
        Conteneur c1 = new Conteneur(100,500);
        Colis colis1 = new Colis(10,200);
        Colis colis2 = new Colis(50,250);
        Colis colis3 = new Colis(60,100);
        
        //Test sur l'ajout de Conteneur
        if (!c1.ajout(colis1)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        if (!c1.ajout(colis2)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        
        //Test verification volume max
        if (c1.ajout(colis3)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        
        //Test sur le cout
        if(c1.cout() != 6000) {
            System.out.println("Probleme sur le cout");
            System.exit(-1);       
        }
        
        System.out.println(c1.toString());
        
        //Création initiale ConteneurUrgent
        ConteneurUrgent c2 = new ConteneurUrgent(200,500,600);
        Colis colis4 = new Colis(20,300);
        Colis colis5 = new Colis(70,200);
        Colis colis6 = new Colis(90,100);
        Colis colis7 = new Colis(90,200);
        
        //Test sur l'ajout de Conteneur
        if (!c2.ajout(colis4)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        if (!c2.ajout(colis5)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        
        //Test verification volume max
        if (c2.ajout(colis6)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        
        //Test verification poids max
        if (c2.ajout(colis7)) {
            System.out.println("Probleme sur l'ajout");
            System.exit(-1);                
        }
        
        //Test sur le cout
        if(c2.cout() != 18000) {
            System.out.println("Probleme sur le cout");
            System.exit(-1);       
        }
        
        System.out.println(c2.toString());
    }  
}
